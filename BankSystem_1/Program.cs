﻿using System;
using System.Collections.Generic;

namespace lesson_7
{
    class Mainclass
    {
        public static void Main(string[] args)
        {
            bool cycle = true;
            while (cycle)
            {
                Console.Clear();
                Console.WriteLine($"{Bank.nameBank} \n{Bank.adress}");
                Console.WriteLine("====================\n\n1.Добавить клиента\n2.Удалить клиента\n3.Изменить баланс по имени\n4.Информация о клиенте\n5.Информация о всех клиентах\n6.Выход\n");
                int num = Convert.ToInt32(Console.ReadLine());

                switch (num)
                {
                    case 1:
                        Bank.addClients();
                        break;
                    case 2:
                        Bank.removeClients();
                        break;
                    case 3:
                        Bank.changeBalance();
                        break;
                    case 4:
                        Bank.showClient();
                        break;
                    case 5:
                        Bank.showAllClient();
                        break;
                    case 6:
                        cycle = false;
                        break;


                }

            }
        }

    }
    class Client
    {
        public string Name;
        public string Surname;
        public int Age;
        public double Balance;

        public Client()
        {
            Name = "name";
            Surname = "surname";
            Age = 0;
            Balance = 0;
        }

        public Client(string name, string surname, int age, double balance)
        {
            Name = name;
            Surname = surname;
            Age = age;
            Balance = balance;
        }

        public void ChangeName(string name) => Name = name;
        public void ChangeBalance(double balance) => Balance = balance;
    }
    class Bank
    {
        public static string nameBank = "AlfaBank";
        public static string adress = "";
        public static List<Client> clientList = new List<Client>();

        public static void addClients()
        {
            Console.Clear();
            Console.WriteLine("1.Введите имя клиента");
            string name = Console.ReadLine();
            Console.WriteLine("2.Введите фамилию клиента");
            string surname = Console.ReadLine();
            Console.WriteLine("3.Введите возраст клиента");
            int age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("4.Введите баланс клиента");
            double balance = Convert.ToDouble(Console.ReadLine());

            Console.Clear();
            Console.WriteLine($"Клиент {surname} {name} добавлен в список клинтов");

            Client client = new Client(name, surname, age, balance);
            clientList.Add(client);

        }
        public static void removeClients()
        {
            Console.Clear();
            Console.WriteLine("Имя клиента для удаления");
            string name = Console.ReadLine();
            for (int i = 0; i < clientList.Count; i++)
            {
                if (name == clientList[i].Name)
                {
                    string clientname = clientList[i].Name;
                    string clientsurname = clientList[i].Surname;
                    clientList.Remove(clientList[i]);
                    Console.WriteLine($"Клиент {clientsurname} {clientname} удалён!");
                    Console.ReadLine();
                }
            }

        }
        public static void changeBalance()
        {
            string name = Console.ReadLine();
            for (int i = 0; i < clientList.Count; i++)
            {
                if (name == clientList[i].Name)
                {
                    Console.WriteLine($"Старый баланс клиента: {clientList[i].Balance}\n Введите новый баланс клиента:");
                    double balance = Convert.ToDouble(Console.ReadLine());
                    clientList[i].Balance = balance;
                }
            }
        }
        public static void showClient()
        {
            Console.Clear();
            Console.WriteLine("Введите имя клиента для получения информации");
            string name = Console.ReadLine();
            for (int i = 0; i < clientList.Count; i++)
            {
                if (name == clientList[i].Name)
                {
                    Console.WriteLine($"Фамилия: {clientList[i].Surname}\nИмя: {clientList[i].Name}\nВозраст: {clientList[i].Age}\nБаланс: {clientList[i].Balance}$");
                }
            }
            Console.ReadLine();

        }
        public static void showAllClient()
        {
            Console.Clear();
            if (clientList.Count == 0)
            {
                Console.WriteLine($"В банке нет клиентов!");
            }
            else
            {
                Console.WriteLine($"Всего клиентов в банке {clientList.Count}\n====================\n");
                clientList.Sort((left, right) => left.Surname.CompareTo(right.Surname));
                foreach (Client sortedClientList in clientList)
                {
                    Console.WriteLine($"Фамилия: {sortedClientList.Surname}\nИмя: {sortedClientList.Name}\nВозраст: {sortedClientList.Age}\nБаланс: {sortedClientList.Balance}\n====================\n");
                }
            }
            Console.WriteLine("Для продолжения нажмите любую кнопку");
            Console.ReadLine();
        }



    }


}
